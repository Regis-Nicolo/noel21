const body = document.body;

setInterval(rainFall, 10);

function rainFall() {
    const snowDrop = document.createElement('i');

    snowDrop.classList.add("far");
    snowDrop.classList.add("fa-snowflake");
    snowDrop.style.fontSize = Math.random() * 7 + 'px';
    snowDrop.style.animationDuration = Math.random() * 2 + 's';
    snowDrop.style.opacity = Math.random() + 0.3;
    snowDrop.style.left = Math.random() * window.innerWidth + 'px';
    
body.appendChild(snowDrop); 
}

setTimeout(() => {
    snowDrop.remove();
}, 6000);